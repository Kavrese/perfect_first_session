import 'package:flutter/material.dart';
import 'package:session_auth/presentation/pages/log_in_screen.dart';
import 'package:session_auth/presentation/theme/colors.dart';
import 'package:session_auth/presentation/theme/theme.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void main() async {
  await Supabase.initialize(
      url: "https://yuetxyqixuhyrazaykjr.supabase.co",
      anonKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inl1ZXR4eXFpeHVoeXJhemF5a2pyIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTExMTEyMDQsImV4cCI6MjAyNjY4NzIwNH0.2479CLxv171jlJ4fSUfhyyqUyeuQY648imw71sECB84"
  );
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  var isLightTheme = true;

  void changeDifferentTheme(BuildContext context){
    isLightTheme = !isLightTheme;
    context.findAncestorStateOfType<_MyAppState>()!.onChangeTheme();
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLightTheme) ? colorsLights : colorsDark;
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? light : dark;
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  void onChangeTheme() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: const LogInPage(),
    );
  }
}