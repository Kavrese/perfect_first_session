import 'package:flutter/material.dart';

abstract class ColorsApp {
  abstract final Color text;
  abstract final Color accent;
  abstract final Color textAccent;
  abstract final Color disableAccent;
  abstract final Color disableTextAccent;
  abstract final Color hint;
  abstract final Color googleOAuth2;
  abstract final Color iconTint;
  abstract final Color background;
  abstract final Color error;
  abstract final Color block;
  abstract final Color subText;
}

class LightColorsApp extends ColorsApp {
  @override
  // TODO: implement accent
  Color get accent => const Color(0xFF6A8BF9);
  @override
  // TODO: implement background
  Color get background => const Color(0xFFFFFFFF);

  @override
  // TODO: implement block
  Color get block => const Color(0xFF222222);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => const Color(0xFFA7A7A7);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => const Color(0xFFFFFFFF);

  @override
  // TODO: implement error
  Color get error => const Color(0xFFFF0000);

  @override
  // TODO: implement googleOAuth2
  Color get googleOAuth2 => const Color(0xFFEC8000);

  @override
  // TODO: implement hint
  Color get hint => const Color(0xFFCFCFCF);

  @override
  // TODO: implement iconTint
  Color get iconTint => const Color(0xFFFFFFFF);

  @override
  // TODO: implement subText
  Color get subText => const Color(0xFF818181);

  @override
  // TODO: implement test
  Color get text => const Color(0xFF3A3A3A);

  @override
  // TODO: implement textAccent
  Color get textAccent => const Color(0xFFFFFFFF);

}

class DarkColorsApp extends ColorsApp {
  @override
  // TODO: implement accent
  Color get accent => const Color(0xFF6A8BF9);
  @override
  // TODO: implement background
  Color get background => const Color(0xFF0D0D0D);

  @override
  // TODO: implement block
  Color get block => const Color(0xFF222222);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => const Color(0xFFA7A7A7);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => const Color(0xFFFFFFFF);

  @override
  // TODO: implement error
  Color get error => const Color(0xFFFF0000);

  @override
  // TODO: implement googleOAuth2
  Color get googleOAuth2 => const Color(0xFFEC8000);

  @override
  // TODO: implement hint
  Color get hint => const Color(0xFF575757);

  @override
  // TODO: implement iconTint
  Color get iconTint => const Color(0xFFFFFFFF);

  @override
  // TODO: implement subText
  Color get subText => const Color(0xFFA7A7A7);

  @override
  // TODO: implement test
  Color get text => const Color(0xFFFFFFFF);

  @override
  // TODO: implement textAccent
  Color get textAccent => const Color(0xFFFFFFFF);
}