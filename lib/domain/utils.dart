import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> request<T>(
  Future<T> Function() request,
  Function(T) onResponse,
  Future<void> Function(String) onError
) async {
  try{
    if (! await checkNetwork()){
      await onError("Network error");
    }else{
      var response = await request();
      onResponse(response);
    }
  } on PostgrestException catch(e) {
    await onError(e.message);
  } on AuthException catch (e) {
    await onError(e.message);
  } catch(e) {
    await onError(e.toString());
  }
}

Future<bool> checkNetwork() async {
  var result = await Connectivity().checkConnectivity();
  return result != ConnectivityResult.none;
}

bool checkEmail(String email){
  return RegExp(r"^[0-9a-z]+@[a-z]+\.[a-z]{2,}$").hasMatch(email);
}

bool checkPassword(String password){
  return password.isNotEmpty;
}