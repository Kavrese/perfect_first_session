import 'package:session_auth/data/models/model_auth.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signIn(
    ModelAuth modelAuth,
    ) async {
  return await supabase.auth.signInWithPassword(
    email: modelAuth.email,
    password: modelAuth.password
  );
}

Future<AuthResponse> signUp(
    ModelAuth modelAuth,
) async {
  return await supabase.auth.signUp(
    email: modelAuth.email,
    password: modelAuth.password
  );
}
